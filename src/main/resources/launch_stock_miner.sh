#!/bin/bash

# Initialize input directory
default_input_dir=input
input_dir=${1:-$default_input_dir}

# Save current directory
current_dir=`pwd`
current_dir=${current_dir// /%20}

# Navigate to Hadoop directory
cd /usr/apache/hadoop/hadoop-2.9.1

# Clean output
bin/hadoop fs -rmr user/fdespotovski/stocks_recommender/intermediate/
bin/hadoop fs -rmr user/fdespotovski/stocks_recommender/output/

# Lauch jar
bin/hadoop jar jStockMiner.jar user/fdespotovski/stocks_recommender/$input_dir/ user/fdespotovski/stocks_recommender/intermediate user/fdespotovski/stocks_recommender/output GOOG MPC SO /media/fdespotovski/OS/Users/dell/Documents/Filip\ FINKI/Sedmi\ semestar/Paralelno\ i\ distribuirano\ procesiranje/Proekt/Results

# Get output file
bin/hadoop fs -get -f user/fdespotovski/stocks_recommender/output/part-r-0000* ${current_dir}/../../Results/
