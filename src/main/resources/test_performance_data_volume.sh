#!/bin/bash

# Iterate over all input directories in arguments and test performance
for i in $@
do
./test_performance.sh 5 $i
done
