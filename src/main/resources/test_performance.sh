#!/bin/bash

# $1 == Number of iterations
# $2 == Input directory
for i in $(eval echo {1..$1})
do
./launch_recommender.sh $2
done
