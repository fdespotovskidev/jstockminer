package main.stockminer;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import main.stockminer.comparator.TextStockGroupingComparator;
import main.stockminer.mapper.SimilarStocksMapper;
import main.stockminer.mapper.StocksOfInterestMapper;
import main.stockminer.model.SimilarStock;
import main.stockminer.io.TextArrayWritable;
import main.stockminer.partitioner.StocksDataPartitioner;
import main.stockminer.reducer.SimilarStocksReducer;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class StockMiner {


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        // Set stocks to compute similar stocks to, contained in a single string, comma separated
        StringBuilder sb = new StringBuilder();
        Set<String> stocksOfInterest = new HashSet<>();

        stocksOfInterest.add(args[3]);
        sb.append(args[3]);
        System.out.printf("args %d: %s\n", 3, args[3]);

        for (int i = 4; i < args.length - 1; i++) {
            stocksOfInterest.add(args[i]);
            sb.append(',').append(args[i]);
            System.out.printf("args %d: %s\n", i, args[i]);
        }

        conf.set("stocksOfInterest", sb.toString());

        Long startTime = System.currentTimeMillis();

        // Map stocks of interest and store them in a file
        Job stocksOfInterestMapperJob = Job.getInstance(conf, "StocksOfInterestMapper");
        stocksOfInterestMapperJob.setJarByClass(StockMiner.class);
        stocksOfInterestMapperJob.setOutputKeyClass(Text.class);
        stocksOfInterestMapperJob.setOutputValueClass(Text.class);
        stocksOfInterestMapperJob.setMapperClass(StocksOfInterestMapper.class);

        stocksOfInterestMapperJob.setInputFormatClass(TextInputFormat.class);
        stocksOfInterestMapperJob.setOutputFormatClass(TextOutputFormat.class);
        stocksOfInterestMapperJob.setNumReduceTasks(0);

        FileInputFormat.addInputPath(stocksOfInterestMapperJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(stocksOfInterestMapperJob, new Path(args[1]));

        stocksOfInterestMapperJob.waitForCompletion(true);

        Job stockMinerJob = Job.getInstance(conf, "main.stockminer.StockMiner");
        stockMinerJob.setJarByClass(StockMiner.class);

        stockMinerJob.setOutputKeyClass(Text.class);
        stockMinerJob.setOutputValueClass(Text.class);
        stockMinerJob.setMapOutputValueClass(TextArrayWritable.class);
        stockMinerJob.setGroupingComparatorClass(TextStockGroupingComparator.class);
        stockMinerJob.setMapperClass(SimilarStocksMapper.class);
        stockMinerJob.setReducerClass(SimilarStocksReducer.class);
        stockMinerJob.setPartitionerClass(StocksDataPartitioner.class);

        stockMinerJob.setInputFormatClass(TextInputFormat.class);
        stockMinerJob.setOutputFormatClass(TextOutputFormat.class);
        stockMinerJob.addCacheFile(new Path(String.format("%s/part-m-00000", args[1])).toUri());
        stockMinerJob.addCacheFile(new Path(String.format("%s/part-m-00001", args[1])).toUri());

        FileInputFormat.addInputPath(stockMinerJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(stockMinerJob, new Path(args[2]));

        stockMinerJob.waitForCompletion(true);

        Long endTime = System.currentTimeMillis();

        Map<String, Map<String, List<Double>>> stocksSimilarityData = new HashMap<>();
        Map<String, Set<String>> stocksSectorsData = new HashMap<>();
        Map<String, TreeSet<SimilarStock>> stockPearsonCorrelation = new HashMap<>();
        Map<String, TreeSet<SimilarStock>> stockSector = new HashMap<>();
        Map<String, TreeSet<SimilarStock>> stockSharpe = new HashMap<>();
        Map<String, TreeSet<SimilarStock>> stockRanked = new HashMap<>();
        TreeSet<SimilarStock> stockRegressionData = new TreeSet<>();
        Map<String, List<Double>> stockExponentialSmoothingData = new HashMap<>();

        stocksOfInterest.stream().forEach(s -> {
            stocksSimilarityData.put(s, new HashMap<>());
            stocksSectorsData.put(s, new HashSet<>());
            stockPearsonCorrelation.put(s, new TreeSet<>());
            stockSector.put(s, new TreeSet<>());
            stockSharpe.put(s, new TreeSet<>());
            stockRanked.put(s, new TreeSet<>());
        });

        FileSystem fileSystem = new Path(args[2]).getFileSystem(conf);

        RemoteIterator<LocatedFileStatus> fileStatusListIterator = fileSystem.listFiles(new Path(args[2]), true);
        while (fileStatusListIterator.hasNext()) {
            LocatedFileStatus fileStatus = fileStatusListIterator.next();

            if (fileStatus.getPath().getName().contains("part-r")) {
                InputStream fileInputStream = fileSystem.open(fileStatus.getPath());
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream))) {
                    bufferedReader.lines().forEach(line -> {
                        String[] lineData = line.replace("\t", "").split(",");
                        Set<String> stocksOfSameSector = new HashSet<>();
                        // Parse stock sectors data
                        int dataIdx = 2;
                        for (; !lineData[dataIdx].equals("pearson"); dataIdx++) {
                            stocksOfSameSector.add(lineData[dataIdx]);
                        }

                        // Parse stock pearson data
                        for (dataIdx = dataIdx + 1; !lineData[dataIdx].equals("sharpe"); dataIdx += 2) {
                            stockPearsonCorrelation.get(lineData[dataIdx]).add(new SimilarStock(lineData[0],
                                    Double.valueOf(lineData[dataIdx + 1])));
                            if (stocksOfSameSector.contains(lineData[dataIdx])) {
                                stockSector.get(lineData[dataIdx]).add(new SimilarStock(lineData[0],
                                        Double.valueOf(lineData[dataIdx + 1])));
                            }
                        }
                        // Parse sharpe ratio data
                        for (dataIdx = dataIdx + 1; dataIdx < lineData.length && !lineData[dataIdx].equals("regression"); dataIdx += 2) {
                            stockSharpe.get(lineData[dataIdx]).add(new SimilarStock(lineData[0], Double.valueOf(lineData[dataIdx + 1])));
                        }

                        // Parse regression data
                        for (dataIdx = dataIdx + 1; dataIdx < lineData.length && !lineData[dataIdx].equals("exponentialSmoothing"); dataIdx++) {
                            stockRegressionData.add(new SimilarStock(lineData[0], Double.valueOf(lineData[dataIdx])));
                        }

                        // Parse exponential smoothing data
                        stockExponentialSmoothingData.put(lineData[0], new ArrayList<>());
                        for (dataIdx = dataIdx + 1; dataIdx < lineData.length; dataIdx++) {
                            stockExponentialSmoothingData.get(lineData[0]).add(Double.valueOf(lineData[dataIdx]));
                        }
                    });
                }
            }
        }

        // Store ranked stocks data based on the weighted average value of their Pearson correlation coefficient
        // and Sharpe ratio
        stocksOfInterest.forEach(stockOfInterest -> {
            stockPearsonCorrelation.get(stockOfInterest).forEach(similarStock -> {
                List<SimilarStock> similarStockSharpe = stockSharpe
                        .get(stockOfInterest).stream().filter(similarStock2 -> similarStock2.getStockName()
                                .equals(similarStock.getStockName())).collect(Collectors.toList());
                if (similarStockSharpe.size() > 0) {
                    Double recommendationIdx = 0.6 * Math.abs(similarStock.getStockValue()) + 0.4 * similarStockSharpe.get(0).getStockValue();
                    stockRanked.get(stockOfInterest).add(new SimilarStock(similarStock.getStockName(), recommendationIdx));
                }

            });
        });

        List<String> resultsWritable = new ArrayList<>();

        // Output most and least similar stocks (Pearson correlation)
        stockPearsonCorrelation.keySet().forEach(stock -> {
                    resultsWritable.add(String.format("Stock of interest: %s. Most similar: %s\n", stock, stockPearsonCorrelation.get(stock).stream().limit(10)
                            .map(SimilarStock::getStockValueString).collect(Collectors.toList())));
                    resultsWritable.add(String.format("Stock of interest: %s. Least similar: %s\n", stock, stockPearsonCorrelation
                            .get(stock).descendingSet().stream().limit(10)
                            .map(SimilarStock::getStockValueString).collect(Collectors.toList())));
                }
        );

        // Output most and least similar stocks (Same sectors + Pearson correlation)
        stockSector.keySet().forEach(stock -> {
                    resultsWritable.add(String.format("Stock of interest: %s. Most similar (same sector): %s\n", stock, stockSector.get(stock).stream().limit(10)
                            .map(SimilarStock::getStockValueString).collect(Collectors.toList())));
                    resultsWritable.add(String.format("Stock of interest: %s. Least similar (same sector): %s\n", stock, stockSector
                            .get(stock).descendingSet().stream().limit(10)
                            .map(SimilarStock::getStockValueString).collect(Collectors.toList())));
                }
        );

        // Output recommended stocks (Sharpe ratio)
        stockSharpe.keySet().forEach(stock ->
                resultsWritable.add(String.format("Stock of interest: %s. Best sharpe ratios: %s\n", stock, stockSharpe.get(stock).stream().limit(10)
                        .map(SimilarStock::getStockValueString).collect(Collectors.toList())))
        );

        // Output ranked stocks
        stockRanked.keySet().forEach(stock ->
                resultsWritable.add(String.format("Stock of interest: %s. Best ranked stocks: %s\n", stock, stockRanked.get(stock).stream().limit(10)
                        .map(SimilarStock::getStockValueString).collect(Collectors.toList())))
        );

        // Output recommended stocks (Regression)
        resultsWritable.add(String.format("Top 10 recommended stocks with best rates of change of their values: %s\n",
                stockRegressionData.stream().limit(10).map(SimilarStock::getStockValueString).collect(Collectors.toList())));

        // Output smoothed stocks (Exponential smoothing)
        int numberOfSmoothedStocksShown = 10;
        int numberOfSmoothedValuesShown = 10;
        stockExponentialSmoothingData.keySet().stream().limit(numberOfSmoothedStocksShown).forEach(stock -> {
                    List<Double> smoothedValues = stockExponentialSmoothingData.get(stock);
                    resultsWritable.add(String.format("Stock: %s. Smoothed values (last %d): %s\n", stock,
                            numberOfSmoothedValuesShown, smoothedValues.stream()
                                    .skip(Math.max(0, smoothedValues.size() - numberOfSmoothedValuesShown))
                                    .collect(Collectors.toList())));
                }

        );

        resultsWritable.add(String.format("Total execution time: %f seconds.\n", (endTime - startTime) / 1000.0));

        // Output and write results to file
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(String.format("%s/program_output.txt", args[args.length - 1])))) {
            for (String result : resultsWritable) {
                System.out.print(result);
                bufferedWriter.write(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Output execution performance data to file
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(String.format("%s/performance_output_laptop.csv", args[args.length - 1]), true))) {
            bufferedWriter.write(String.format("%d,%f\n", stocksOfInterest.size(), (endTime - startTime) / 1000.0));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}