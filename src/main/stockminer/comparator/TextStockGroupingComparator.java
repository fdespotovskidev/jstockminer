package main.stockminer.comparator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class TextStockGroupingComparator extends WritableComparator {
    public TextStockGroupingComparator() {
        super(Text.class, true);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public int compare(WritableComparable w1, WritableComparable w2) {
        Text key1 = (Text) w1;
        Text key2 = (Text) w2;
        String[] key1Data = key1.toString().split(",");
        String[] key2Data = key2.toString().split(",");
        return key1Data[0].equals(key2Data[0]) ? 0 : 1;
    }
}