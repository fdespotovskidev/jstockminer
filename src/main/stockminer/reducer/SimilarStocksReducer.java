package main.stockminer.reducer;

import main.stockminer.io.TextArrayWritable;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

public class SimilarStocksReducer extends Reducer<Text, TextArrayWritable, Text, Text> {

    private Configuration conf;
    private Set<String> stocksOfInterest;
    private java.util.Map<String, Set<String>> stocksOfInterestDates;
    private java.util.Map<String, List<String>> stocksOfInterestDatesLists;
    private java.util.Map<String, List<Double>> stocksOfInterestData;
    private Map<String, String> stocksOfInterestSectors;
    private Set<String> stocksOfInterestSharedDates;
    private int stockDataOfInterestIdx;
    private SimpleRegression regressionModel;

    private void parseStocksOfInterestData(String cacheDir) {
        try (BufferedReader reader = new BufferedReader(new FileReader(cacheDir))) {
            reader.lines().forEachOrdered(line -> {
                String[] lineData = line.replace("\t", "").split(",");
                // Check if it's sector or data
                if (lineData.length == 2) {
                    // Put stock sector in map
                    stocksOfInterestSectors.put(lineData[0], lineData[1]);
                } else {
                    // Process stock data

                    // Set stock of interest date
                    if (!stocksOfInterestDates.containsKey(lineData[0])) {
                        stocksOfInterestDates.put(lineData[0], new HashSet<>());
                    }
                    stocksOfInterestDates.get(lineData[0]).add(lineData[1]);

                    if (!stocksOfInterestDatesLists.containsKey(lineData[0])) {
                        stocksOfInterestDatesLists.put(lineData[0], new ArrayList<>());
                    }
                    stocksOfInterestDatesLists.get(lineData[0]).add(lineData[1]);

                    // Set stock of interest data of interest
                    if (!stocksOfInterestData.containsKey(lineData[0])) {
                        stocksOfInterestData.put(lineData[0], new ArrayList<>());
                    }
                    stocksOfInterestData.get(lineData[0]).add(Double.parseDouble(lineData[2 + stockDataOfInterestIdx]));
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setup(Context context) throws IOException {
        conf = context.getConfiguration();

        // Set stocks of interest
        stocksOfInterest = new HashSet<>();

        String[] stocks = conf.get("stocksOfInterest").split(",");
        for (String stock : stocks) {
            System.out.printf("Stock of interest: %s\n", stock);
            stocksOfInterest.add(stock);
        }

        // Initialize stock data of interest
        stockDataOfInterestIdx = 0;

        // Initialize stocks of interest data containers
        stocksOfInterestData = new HashMap<>();
        stocksOfInterestDates = new HashMap<>();
        stocksOfInterestDatesLists = new HashMap<>();
        stocksOfInterestSectors = new HashMap<>();
        stocksOfInterestSharedDates = new HashSet<>();

        // Read and parse stocks of interest data from cache
        URI[] cacheFilesPatternsURIs = Job.getInstance(conf).getCacheFiles();
        for (URI patternsURI : cacheFilesPatternsURIs) {
            Path patternsPath = new Path(patternsURI.getPath());
            parseStocksOfInterestData(patternsPath.getName());
        }

        // Initialize shared dates container
        boolean firstStock = true;
        for (String stockOfInterest : stocksOfInterest) {
            if (firstStock) {
                firstStock = false;
                stocksOfInterestSharedDates.addAll(stocksOfInterestDates.get(stockOfInterest));
            } else {
                stocksOfInterestSharedDates.retainAll(stocksOfInterestDates.get(stockOfInterest));
            }
        }

        // Initialize regression main.stockminer.model
        regressionModel = new SimpleRegression();
    }

    private Double calculatePearsonCorrelation(List<Double> stockOfInterestData, List<Double> stockData,
                                               List<String> stockDataDatesList, Set<String> stockDataDates,
                                               String stockOfInterestName) {
        Set<String> stockOfInterestDataDates = stocksOfInterestDates.get(stockOfInterestName);
        List<String> stockOfInterestDataDatesList = stocksOfInterestDatesLists.get(stockOfInterestName);

        List<Double> stockOfInterestFilteredData = new ArrayList<>();
        List<Double> stockFilteredData = new ArrayList<>();

        for (int i = 0; i < stockOfInterestData.size(); i++) {
            if (stockDataDates.contains(stockOfInterestDataDatesList.get(i))) {
                stockOfInterestFilteredData.add(stockOfInterestData.get(i));
            }
        }

        for (int i = 0; i < stockData.size(); i++) {
            if (stockOfInterestDataDates.contains(stockDataDatesList.get(i))) {
                stockFilteredData.add(stockData.get(i));
            }
        }

        Double commonSum = 0.0;
        Double stockOfInterestSum = 0.0;
        Double stockOfInterestSquaredSum = 0.0;
        Double stockSum = 0.0;
        Double stockSquaredSum = 0.0;

        for (int i = 0; i < stockOfInterestFilteredData.size(); i++) {
            commonSum += stockOfInterestFilteredData.get(i) * stockFilteredData.get(i);
            stockOfInterestSum += stockOfInterestFilteredData.get(i);
            stockOfInterestSquaredSum += stockOfInterestFilteredData.get(i) * stockOfInterestFilteredData.get(i);
            stockSum += stockFilteredData.get(i);
            stockSquaredSum += stockFilteredData.get(i) * stockFilteredData.get(i);
        }

        Double denominator = Math.sqrt((stockOfInterestSquaredSum - ((stockOfInterestSum * stockOfInterestSum) / stockFilteredData.size())) *
                (stockSquaredSum - ((stockSum * stockSum) / stockFilteredData.size())));
        return denominator == 0.0 ? 0.0 : (commonSum - ((stockOfInterestSum * stockSum) / stockFilteredData.size())) / denominator;
    }

    /**
     * Calculates sharpe ratios for each portfolio of current stock and stocks of interests. Fits regression main.stockminer.model
     * to current stock data.
     *
     * @param stockData
     * @param stockDataDatesList
     * @param stockDataDates
     * @return
     */
    private Map<String, Map<String, List<Double>>> calculateStockDescriptors(List<Double> stockData, List<String> stockDataDatesList,
                                                                             Set<String> stockDataDates) {
        List<Double> stockFilteredData = new ArrayList<>();

        Map<String, List<Double>> stocksOfInterestFilteredData = new HashMap<>();
        Map<String, Double> stocksOfInterestFilteredDataDailyReturnAverages = new HashMap<>();
        Map<String, Double> stocksOfInterestFilteredDataExpectedReturns = new HashMap<>();

        for (String stockOfInterest : stocksOfInterest) {
            stocksOfInterestFilteredData.put(stockOfInterest, new ArrayList<>());
            Double stockOfInterestDailyReturnAverage = 0.0;
            for (int i = 0; i < stocksOfInterestData.get(stockOfInterest).size(); i++) {
                if (stockDataDates.contains(stocksOfInterestDatesLists.get(stockOfInterest).get(i))) {
                    stocksOfInterestFilteredData.get(stockOfInterest).add(stocksOfInterestData.get(stockOfInterest).get(i));
                    int numberOfFilteredElements = stocksOfInterestFilteredData.get(stockOfInterest).size();
                    if (numberOfFilteredElements > 1) {
                        stockOfInterestDailyReturnAverage +=
                                (stocksOfInterestFilteredData.get(stockOfInterest).get(numberOfFilteredElements - 1)
                                        - stocksOfInterestFilteredData.get(stockOfInterest).get(numberOfFilteredElements - 2))
                                        / stocksOfInterestFilteredData.get(stockOfInterest).get(numberOfFilteredElements - 2);
                    }
                }
            }

            if (stocksOfInterestFilteredData.get(stockOfInterest).size() < 2) {
                return null;
            }

            stockOfInterestDailyReturnAverage /= stocksOfInterestFilteredData.get(stockOfInterest).size() - 1;
            stocksOfInterestFilteredDataDailyReturnAverages.put(stockOfInterest, stockOfInterestDailyReturnAverage);
            // Expected return is annualized average daily return (multiplied by 252 (tradeable days))
            stocksOfInterestFilteredDataExpectedReturns.put(stockOfInterest, stockOfInterestDailyReturnAverage * 252);
        }

        // Reset regression main.stockminer.model
        regressionModel.clear();

        // Declare exponential smoothing variables
        Double exponentialSmoothingConstant = 0.15;
        Double trendConstant = 0.15;
        Double previousValue = null;
        Double previousValuePredicted = null;
        Double previousValueTrend = null;
        Double previousValuePredictedTrendAdjusted = null;

        Double stockDailyReturnAverage = 0.0;
        List<Double> exponentialSmoothingValues = new ArrayList<>();
        for (int i = 0; i < stockData.size(); i++) {
            if (stocksOfInterestSharedDates.contains(stockDataDatesList.get(i))) {
                stockFilteredData.add(stockData.get(i));
                int numberOfFilteredElements = stockFilteredData.size();
                if (numberOfFilteredElements > 1) {
                    stockDailyReturnAverage += (stockFilteredData.get(numberOfFilteredElements - 1)
                            - stockFilteredData.get(numberOfFilteredElements - 2))
                            / stockFilteredData.get(numberOfFilteredElements - 2);
                }

            }

            // Append data to regression main.stockminer.model
            regressionModel.addData(i, stockData.get(i));

            // Calculate exponential smoothing values

            if (previousValuePredicted == null) {
                previousValuePredicted = stockData.get(i);
                previousValueTrend = 0.0;
            } else {
                previousValuePredicted = exponentialSmoothingConstant * previousValue
                        + (1 - exponentialSmoothingConstant) * previousValuePredictedTrendAdjusted;
                previousValueTrend += trendConstant * (previousValuePredicted - previousValuePredictedTrendAdjusted);
            }
            previousValue = stockData.get(i);
            previousValuePredictedTrendAdjusted = previousValuePredicted + previousValueTrend;
            exponentialSmoothingValues.add(previousValuePredictedTrendAdjusted);
        }

        stockDailyReturnAverage /= stockFilteredData.size() - 1;
        // Expected return is annualized average daily return (multiplied by 252 (tradeable days))
        Double stockExpectedReturns = stockDailyReturnAverage * 252;

        Map<String, Double> covsStocksOfInterestDaily = new HashMap<>();
        Map<String, Double> varsStocksOfInterestDaily = new HashMap<>();
        Double varStockDaily = 0.0;

        for (int i = 1; i < stockFilteredData.size(); i++) {
            Double dailyReturn = (stockFilteredData.get(i) - stockFilteredData.get(i - 1)) / stockFilteredData.get(i - 1);
            Double partialVar = dailyReturn - stockDailyReturnAverage;
            varStockDaily += partialVar * partialVar;

            for (String stockOfInterest : stocksOfInterest) {
                Double stockOfInterestDailyReturn = (stocksOfInterestFilteredData.get(stockOfInterest).get(i)
                        - stocksOfInterestFilteredData.get(stockOfInterest).get(i - 1))
                        / stocksOfInterestFilteredData.get(stockOfInterest).get(i - 1);
                Double partialCov = stockOfInterestDailyReturn - stocksOfInterestFilteredDataDailyReturnAverages.get(stockOfInterest);
                if (!covsStocksOfInterestDaily.containsKey(stockOfInterest)) {
                    covsStocksOfInterestDaily.put(stockOfInterest, partialCov * partialVar);
                    varsStocksOfInterestDaily.put(stockOfInterest, partialCov * partialCov);
                } else {
                    Double curPartialCov = covsStocksOfInterestDaily.get(stockOfInterest);
                    Double curPartialVar = varsStocksOfInterestDaily.get(stockOfInterest);

                    covsStocksOfInterestDaily.replace(stockOfInterest, curPartialCov + partialCov * partialVar);
                    varsStocksOfInterestDaily.replace(stockOfInterest, curPartialVar + partialCov * partialCov);
                }
            }
        }

        varStockDaily /= stockFilteredData.size() - 1;
        covsStocksOfInterestDaily.keySet().forEach(stockOfInterest -> {
            Double cov = covsStocksOfInterestDaily.get(stockOfInterest) / (stockFilteredData.size() - 1);
            Double var = varsStocksOfInterestDaily.get(stockOfInterest) / (stockFilteredData.size() - 1);

            covsStocksOfInterestDaily.replace(stockOfInterest, cov);
            varsStocksOfInterestDaily.replace(stockOfInterest, var);
        });


        int lastElementIdx = stockFilteredData.size() - 1;

        // Calculate annualized expected returns and standard deviations of portfolios
        // Construct a map of stocks of interest and their pairwise sharpe ratios with current stock
        // 0.0223 risk free rate

        Map<String, List<Double>> sharpeRatios = new HashMap<>();
        for (String stockOfInterest : stocksOfInterest) {
            Double stockOfInterestWeight = stocksOfInterestFilteredData.get(stockOfInterest).get(lastElementIdx);
            Double stockOfInterestWeightSquared = stockOfInterestWeight * stockOfInterestWeight;
            Double stockWeight = stockFilteredData.get(lastElementIdx);
            Double stockWeightSquared = stockWeight * stockWeight;

            Double portfolioAnnualizedExpectedReturn = stockOfInterestWeight * stocksOfInterestFilteredDataExpectedReturns.get(stockOfInterest)
                    + stockWeight * stockExpectedReturns;

            Double portfolioVariance = stockOfInterestWeightSquared * varsStocksOfInterestDaily.get(stockOfInterest)
                    + stockWeightSquared * varStockDaily
                    + 2 * stockOfInterestWeight * stockWeight * covsStocksOfInterestDaily.get(stockOfInterest);
            // Annualize portfolio variance
            portfolioVariance *= 252;

            Double portfolioSharpeRatio = (portfolioAnnualizedExpectedReturn - 0.0223) / Math.sqrt(portfolioVariance);
            List<Double> sharpeValue = new ArrayList<>();
            sharpeValue.add(portfolioSharpeRatio);
            sharpeRatios.put(stockOfInterest, sharpeValue);
        }

        // Fit regression main.stockminer.model
        regressionModel.regress();

        List<Double> predictedValues = new ArrayList<>();

        // Store regression line slope
        predictedValues.add(regressionModel.getSlope());

        // Store exponential smoothing data
        Map<String, List<Double>> regressionValues = new HashMap<>();
        regressionValues.put("regressionValues", predictedValues);

        Map<String, List<Double>> exponentialSmoothingData = new HashMap<>();
        exponentialSmoothingData.put("exponentialSmoothingValues", exponentialSmoothingValues);

        Map<String, Map<String, List<Double>>> calculatedData = new HashMap<>();
        calculatedData.put("sharpe", sharpeRatios);
        calculatedData.put("regression", regressionValues);
        calculatedData.put("exponentialSmoothing", exponentialSmoothingData);

        return calculatedData;
    }

    public void reduce(Text key, Iterable<TextArrayWritable> values, Context context)
            throws IOException, InterruptedException {
        Iterator<TextArrayWritable> stockDataIterator = values.iterator();
        String stockSector = null;
        List<Double> stockData = new ArrayList<>();
        Set<String> stockDataDates = new HashSet<>();
        List<String> stockDataDatesList = new ArrayList<>();

        String stockName = key.toString().split(",")[0];

        while (stockDataIterator.hasNext()) {
            Writable[] curStockData = stockDataIterator.next().get();
            if (curStockData.length == 1) {
                stockSector = curStockData[0].toString();
            } else {
                // Store date of stock
                stockDataDates.add(curStockData[curStockData.length - 1].toString());
                stockDataDatesList.add(curStockData[curStockData.length - 1].toString());

                for (int i = 0; i < curStockData.length - 1; i++) {
                    // Store stock data of interest into array
                    if (i == stockDataOfInterestIdx) {
                        stockData.add(Double.valueOf(curStockData[i].toString()));
                    }
                }
            }
        }

        // Calculate similarity metrics with stocks of interest
        List<Map<String, Double>> stocksSimilarities = stocksOfInterest.stream().map(stock -> {
            Map<String, Double> similarities = new HashMap<>();
            List<Double> stockOfInterestData = stocksOfInterestData.get(stock);
            Double pearsonCorrelation = calculatePearsonCorrelation(stockOfInterestData, stockData, stockDataDatesList, stockDataDates, stock);

            similarities.put(stock, pearsonCorrelation);
            return similarities;
        }).collect(Collectors.toList());

        // Construct stringbuilder that contains similarity measures to each stock of interest and sharpe ratio
        StringBuilder similaritiesStringbuilder = new StringBuilder();

        // Write sectors data to stringbuilder
        similaritiesStringbuilder.append(",sectors");
        final String stockSectorFinal = stockSector;
        stocksOfInterest.forEach(stock -> {
            if (stockSectorFinal.equals(stocksOfInterestSectors.get(stock))) {
                similaritiesStringbuilder.append(',').append(stock);
            }
        });

        similaritiesStringbuilder.append(",pearson");
        for (int i = 0; i < stocksSimilarities.size(); i++) {
            Map<String, Double> similarity = stocksSimilarities.get(i);
            similarity.keySet().forEach(s ->
                    similaritiesStringbuilder.append(",").append(s).append(",").append(similarity.get(s)));
        }

        // Calculate sharpe ratios of stock for each stock of interest and fit regression main.stockminer.model
        Map<String, Map<String, List<Double>>> sharpeRatiosAndRegressionValues = calculateStockDescriptors(stockData,
                stockDataDatesList, stockDataDates);

        // Write sharpe ratio data to stringbuilder
        similaritiesStringbuilder.append(",sharpe");
        if (sharpeRatiosAndRegressionValues != null) {
            sharpeRatiosAndRegressionValues.get("sharpe").keySet().forEach(s -> similaritiesStringbuilder.append(",")
                    .append(s).append(",").append(sharpeRatiosAndRegressionValues.get("sharpe").get(s).get(0)));
        }

        // Write regression data to stringbuilder
        similaritiesStringbuilder.append(",regression");
        if (sharpeRatiosAndRegressionValues != null) {
            List<Double> regressionValues = sharpeRatiosAndRegressionValues.get("regression").get("regressionValues");
            regressionValues.forEach(regressionValue -> similaritiesStringbuilder.append(",").append(regressionValue));
        }

        // Write exponential smoothing data to stringbuilder
        similaritiesStringbuilder.append(",exponentialSmoothing");
        if (sharpeRatiosAndRegressionValues != null) {
            List<Double> exponentialSmoothingValues = sharpeRatiosAndRegressionValues.get("exponentialSmoothing")
                    .get("exponentialSmoothingValues");
            exponentialSmoothingValues.forEach(value -> similaritiesStringbuilder.append(",").append(value));
        }

        // Write stock data to context
        context.write(new Text(stockName), new Text(similaritiesStringbuilder.toString()));
    }
}