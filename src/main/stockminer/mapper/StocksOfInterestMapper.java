package main.stockminer.mapper;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class StocksOfInterestMapper extends Mapper<LongWritable, Text, Text, Text> {

    private Configuration conf;
    private Set<String> stocksOfInterest;

    @Override
    public void setup(Context context) {
        conf = context.getConfiguration();

        stocksOfInterest = new HashSet<>();

        String[] stocks = conf.get("stocksOfInterest").split(",");
        for (String stock : stocks) {
            System.out.printf("Stock of interest: %s\n", stock);
            stocksOfInterest.add(stock);
        }
    }

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // Skip file header
        if (key.get() == 0) {
            return;
        }
        String line = value.toString();
        try {
            String[] stockData = line.split(",");

            // Check if stock is of interest
            if (!stocksOfInterest.contains(stockData[stockData.length - 1])) {
                return;
            }

            // Check if data is incomplete
            for (int i = 0; i < stockData.length; i++) {
                if (stockData[i].isEmpty()) {
                    return;
                }
            }

            // Check if reading stock data file, or categories file
            if (stockData.length > 3) {
                StringBuilder stockDataSB = new StringBuilder();
                for (int i = 1; i < stockData.length - 2; i++) {
                    stockDataSB.append(",").append(stockData[i]);
                }

                // Write stock data to context
                context.write(new Text(String.format("%s,%s", stockData[stockData.length - 1], stockData[0])),
                        new Text(stockDataSB.toString()));
            } else {
                // Write stock sector to context
                context.write(new Text(stockData[stockData.length - 1]), new Text(String.format(",%s", stockData[1])));
            }
        } catch (Exception e) {
            System.err.printf("EXCEPTION LINE: %s\n", line);
            throw e;
        }
    }
}