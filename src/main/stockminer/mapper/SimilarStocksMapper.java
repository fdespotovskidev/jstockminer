package main.stockminer.mapper;

import main.stockminer.io.TextArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SimilarStocksMapper extends Mapper<LongWritable, Text, Text, TextArrayWritable> {

    private Configuration conf;
    private Set<String> stocksOfInterest;

    @Override
    public void setup(Context context) {
        conf = context.getConfiguration();

        stocksOfInterest = new HashSet<>();

        String[] stocks = conf.get("stocksOfInterest").split(",");
        for (String stock : stocks) {
            System.out.printf("Stock of interest: %s\n", stock);
            if (!stock.isEmpty()) {
                stocksOfInterest.add(stock);
            }
        }
    }

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // Skip file header
        if (key.get() == 0) {
            return;
        }
        String line = value.toString();
        try {
            String[] stockData = line.split(",");

            // Check if data is incomplete
            for (int i = 0; i < stockData.length; i++) {
                if (stockData[i].isEmpty()) {
                    return;
                }
            }

            // Check if stock is of interest
            if (stocksOfInterest.contains(stockData[stockData.length - 1])) {
                return;
            }

            TextArrayWritable stockDataMapped = new TextArrayWritable();

            // Check if reading stock data file, or categories file
            if (stockData.length > 3) {
                Text[] stockValues = new Text[stockData.length - 2];
                for (int i = 1; i < stockData.length - 2; i++) {
                    stockValues[i - 1] = new Text(stockData[i]);
                }
                // Set record date
                stockValues[stockValues.length - 1] = new Text(stockData[0]);
                stockDataMapped.set(stockValues);
            } else if (stockData.length < 2) {
                return;
            } else {
                stockDataMapped.set(new Text[]{new Text(stockData[1])});
            }

            // Write stock data to context
            context.write(new Text(String.format("%s,%s", stockData[stockData.length - 1], stockData[0])), stockDataMapped);
        } catch (Exception e) {
            System.err.printf("EXCEPTION LINE: %s\n", line);
            throw e;
        }
    }

    public Collection<String> getStocksOfInterest() {
        return stocksOfInterest;
    }
}