package main.stockminer.partitioner;

import main.stockminer.io.TextArrayWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class StocksDataPartitioner extends Partitioner<Text, TextArrayWritable> {
    @Override
    public int getPartition(Text key, TextArrayWritable value, int numReduceTasks) {
        String stockName = key.toString().split(",")[0];
        return stockName.hashCode() % numReduceTasks;
    }
}