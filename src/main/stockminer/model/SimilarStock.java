package main.stockminer.model;

import java.util.ArrayList;
import java.util.List;

public class SimilarStock implements Comparable {
    private String stockName;
    private Double stockValue;
    private List<Double> stockData;

    public SimilarStock(String stockName, Double stockValue) {
        this.stockName = stockName;
        this.stockValue = stockValue;
        this.stockData = new ArrayList<>();
    }

    @Override
    public int compareTo(Object o) {
        SimilarStock similarStock = (SimilarStock) o;
        return similarStock.stockValue.compareTo(stockValue);
    }

    public String getStockValueString() {
        return String.format("%s = %f", stockName, stockValue);
    }

    public String getStockName() {
        return stockName;
    }

    public Double getStockValue() {
        return stockValue;
    }
}