package stockminer;

import main.stockminer.comparator.TextStockGroupingComparator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextStockGroupingComparatorTests {

    private TextStockGroupingComparator textStockGroupingComparator;

    @BeforeEach
    public void init() {
        textStockGroupingComparator = new TextStockGroupingComparator();
    }


    @Test
    public void testCompare() {
        WritableComparable w1 = new Text("abc");
        WritableComparable w2 = new Text("abc");
        assertEquals(0, textStockGroupingComparator.compare(w1, w2));
    }

}
