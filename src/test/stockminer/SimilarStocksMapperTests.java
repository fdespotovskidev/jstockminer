package stockminer;

import main.stockminer.io.TextArrayWritable;
import main.stockminer.mapper.SimilarStocksMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SimilarStocksMapperTests {

    private static SimilarStocksMapper ssm = new SimilarStocksMapper();

    @Mock
    private Mapper.Context context;

    private Configuration configuration;

    private Configuration stockConfiguration;

    private ArgumentCaptor valueCapture;

    private static final String stocksOfInterestDir = "src/test/resources/stocksOfInterest.txt";
    private static final String RESOURCES_DIR = "src/test/resources/";

    private Set<String> processStocksOfInterest(String fileDir, Configuration configuration, int lineLimit) {
        StringBuilder stocksOfInterest = new StringBuilder();
        boolean firstStock = true;

        Set<String> stocksOfInterestSet = new HashSet<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDir))) {
            String line;
            int lineCount = 0;
            lineLimit = lineLimit < 0 ? Integer.MAX_VALUE : lineLimit;
            while (lineCount < lineLimit && (line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                if (!firstStock) {
                    stocksOfInterest.append(",");
                } else {
                    firstStock = false;
                }
                stocksOfInterest.append(line);
                stocksOfInterestSet.add(line);
                lineCount++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        configuration.set("stocksOfInterest", stocksOfInterest.toString());
        return stocksOfInterestSet;
    }

    private List<String> processStocksData(String fileDir, int lineLimit) {
        List<String> stocksDataLines = new ArrayList<>();
        boolean firstLine = true;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDir))) {
            String line;
            int lineCount = 0;
            lineLimit = lineLimit < 0 ? Integer.MAX_VALUE : lineLimit;
            while (lineCount < lineLimit && (line = bufferedReader.readLine()) != null) {
                if (firstLine) {
                    firstLine = false;
                }
                lineCount++;
                stocksDataLines.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stocksDataLines;
    }

    @BeforeAll
    public void setup() {
        stockConfiguration = new Configuration();
        processStocksOfInterest(stocksOfInterestDir, stockConfiguration, 3);
    }

    @BeforeEach
    public void init() throws IOException, InterruptedException {
        Mockito.lenient().when(context.getConfiguration()).thenReturn(stockConfiguration);
        ssm.setup(context);
        valueCapture = ArgumentCaptor.forClass(Configuration.class);
        Mockito.lenient().doNothing().when(this.context).write(Mockito.any(Text.class), Mockito.any(TextArrayWritable.class));
    }

    private Collection<Object[]> ispTestOnSetupMethodSource() {
        List<Object[]> tests = new ArrayList<>();
        // Test base choice
        Configuration configuration = new Configuration();
        Set<String> stocksSet = processStocksOfInterest(stocksOfInterestDir, configuration, 6);
        tests.add(new Object[]{configuration, stocksSet});

        // Test 7
        stocksSet = new HashSet<>();
        stocksSet.add("GOOG");
        stocksSet.add("MPC");
        configuration = new Configuration();
        configuration.set("stocksOfInterest", "GOOG,GOOG,MPC");
        tests.add(new Object[]{configuration, stocksSet});

        return tests;
    }

    private Collection<Object[]> graphCoverageTestOnSetupMethodSource() {
        List<Object[]> tests = new ArrayList<>();
        // Test 1
        Configuration configuration = new Configuration();
        Set<String> stocksSet = processStocksOfInterest(stocksOfInterestDir, configuration, 2);
        tests.add(new Object[]{configuration, stocksSet});

        // Test 2
        configuration = new Configuration();
        stocksSet = processStocksOfInterest(stocksOfInterestDir, configuration, 0);
        tests.add(new Object[]{configuration, stocksSet});

        return tests;
    }

    private Collection<Object[]> dataFlowCoverageTestOnSetupMethodSource() {
        List<Object[]> tests = new ArrayList<>();
        // Test 1
        Configuration configuration = new Configuration();
        Set<String> stocksSet = processStocksOfInterest(stocksOfInterestDir, configuration, 1);
        tests.add(new Object[]{configuration, stocksSet});

        // Test 2
        configuration = new Configuration();
        stocksSet = processStocksOfInterest(stocksOfInterestDir, configuration, 0);
        tests.add(new Object[]{configuration, stocksSet});

        return tests;
    }

    public Text getExpectedStockDataKey(String stockData) {
        String[] data = stockData.split(",");
        return new Text(String.format("%s,%s", data[data.length - 1], data[0]));
    }

    public TextArrayWritable getExpectedStockDataValue(String stockData) {
        String[] data = stockData.split(",");
        TextArrayWritable expectedValue = new TextArrayWritable();

        if (data.length > 3) {
            Text[] stockValues = new Text[data.length - 2];
            for (int i = 1; i < data.length - 2; i++) {
                stockValues[i - 1] = new Text(data[i]);
            }
            stockValues[stockValues.length - 1] = new Text(data[0]);
            expectedValue.set(stockValues);
        } else {
            expectedValue.set(new Text[]{new Text(data[1])});
        }

        return expectedValue;
    }

    private Collection<Object[]> ispTestOnMapMethodSource() {
        List<Object[]> tests = new ArrayList<>();

        List<String> stocksDataLines = processStocksData(String.format("%s%s", RESOURCES_DIR,
                "all_stocks_5yr_updated_30_12_2018.csv"), 2);
        List<String> stocksDataLinesMissingValues = processStocksData(String.format("%s%s", RESOURCES_DIR,
                "stocks_data_lines_missing_values.csv"), 1);
        List<String> stocksSectorsLines = processStocksData(String.format("%s%s", RESOURCES_DIR, "stocks_sectors.csv"),
                2);

        Text expectedKey;
        TextArrayWritable expectedValue;
        // Base choice
        expectedKey = getExpectedStockDataKey(stocksDataLines.get(1));
        expectedValue = getExpectedStockDataValue(stocksDataLines.get(1));
        tests.add(new Object[]{new LongWritable(1), new Text(stocksDataLines.get(1)), expectedKey, expectedValue, 1});

        // Test 2
        tests.add(new Object[]{new LongWritable(0), new Text(stocksDataLines.get(0)), null, null, 0});

        // Test 6
        tests.add(new Object[]{new LongWritable(1), new Text(stocksDataLinesMissingValues.get(0)), null, null, 0});

        // Test 7
        String stockDataString = "2014-04-08,542.6,555,541.61,554.9,3152406,GOOG";
        tests.add(new Object[]{new LongWritable(1), new Text(stockDataString), null, null, 0});

        // Test 8
        expectedKey = getExpectedStockDataKey(stocksSectorsLines.get(1));
        expectedValue = getExpectedStockDataValue(stocksSectorsLines.get(1));
        tests.add(new Object[]{new LongWritable(1), new Text(stocksSectorsLines.get(1)), expectedKey, expectedValue, 1});

        return tests;
    }

    private Collection<Object[]> graphCoverageTestOnMapMethodSource() {
        List<Object[]> tests = new ArrayList<>();
        String stockDataString;

        // Test 4
        stockDataString = "2014-04-08,542.6,555,GOOG";
        tests.add(new Object[]{new LongWritable(1), new Text(stockDataString), null, null, 0});

        // Test 6
        stockDataString = "GOOG";
        tests.add(new Object[]{new LongWritable(1), new Text(stockDataString), null, null, 0});

        // Test 8
        stockDataString = "92";
        tests.add(new Object[]{new LongWritable(1), new Text(stockDataString), null, null, 0});

        // Test 9
        stockDataString = ",542.6,555,541.61,554.9,3152406,GOOG";
        tests.add(new Object[]{new LongWritable(1), new Text(stockDataString), null, null, 0});

        // Test 13 (data flow coverage)
        stockDataString = "92,,222,GOOG";
        tests.add(new Object[]{new LongWritable(1), new Text(stockDataString), null, null, 0});

        return tests;
    }

    @ParameterizedTest
    @MethodSource({"ispTestOnSetupMethodSource", "graphCoverageTestOnSetupMethodSource",
            "dataFlowCoverageTestOnSetupMethodSource"})
    public void testsOnSetup(Configuration configuration, Set<String> expectedSet) {
        this.configuration = configuration;
        Mockito.lenient().when(context.getConfiguration()).thenReturn(this.configuration);
        ssm.setup(context);
        Assertions.assertEquals(expectedSet, ssm.getStocksOfInterest());
    }

    @Test
    public void ispTestOnSetup2() {
        Assertions.assertThrows(NullPointerException.class, () -> ssm.setup(null));
    }

    @ParameterizedTest
    @MethodSource({"ispTestOnMapMethodSource", "graphCoverageTestOnMapMethodSource"})
    public void testsOnMap(LongWritable key, Text value, Text expectedKey, TextArrayWritable expectedValue,
                           int numberOfInvocationsOfWrite) throws IOException, InterruptedException {
        ssm.map(key, value, this.context);
        Mockito.verify(this.context, Mockito.times(numberOfInvocationsOfWrite)).write(valueCapture.capture(),
                valueCapture.capture());

        if (numberOfInvocationsOfWrite > 0) {
            List<Object> valueCaptureValues = valueCapture.getAllValues();
            Assertions.assertEquals(expectedKey, valueCaptureValues.get(0));
            Writable[] expectedValues = expectedValue.get();
            Writable[] actualValues = ((TextArrayWritable) valueCaptureValues.get(1)).get();
            Assertions.assertEquals(expectedValues.length, actualValues.length);

            for (int i = 0; i < expectedValues.length; i++) {
                Assertions.assertEquals(expectedValues[i], actualValues[i]);
            }
        }
    }

    @Test
    public void testOnMapKeytNull() {
        // Test 1 (ISP)
        String stockDataString = "2014-04-08,542.6,555,541.61,554.9,3152406,GM";
        Assertions.assertThrows(NullPointerException.class, () ->
                ssm.map(null, new Text(stockDataString), this.context));
    }

    @Test
    public void testOnMapValueNull() {
        // Test 5 (Graph coverage)
        Assertions.assertThrows(NullPointerException.class, () ->
                ssm.map(new LongWritable(1), null, this.context));
    }

    @Test
    public void testOnMapContextNull() {
        // Test 5 (ISP)
        String stockDataString = "2014-04-08,542.6,555,541.61,554.9,3152406,GM";
        Assertions.assertThrows(NullPointerException.class, () ->
                ssm.map(new LongWritable(1), new Text(stockDataString), null));
    }

}
